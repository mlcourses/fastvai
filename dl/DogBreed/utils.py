import subprocess, shutil

from fastai.imports import *
from fastai.conv_learner import *
from fastai.dataset import *
from fastai.transforms import *
from fastai.model import *

from pathlib import Path
from contextlib import contextmanager

shrun = lambda cmd: print(subprocess.getoutput(cmd))

is_empty = lambda gen: next(gen, None) is None

def _make(self):
    self.mkdir(parents=True, exist_ok=True)
    return self
Path.make = _make

COMPETITION = 'dog-breed-identification'
DIR_DATA = (Path('~/.kaggle/Competitions').expanduser() / COMPETITION).make()
DIR_DATA_SAMPLE = (DIR_DATA / 'sample').make()
DIR_CHECKPOINTS = (Path.cwd() / 'Checkpoints').make()

@contextmanager
def workdir(path):
    path_cwd = os.getcwd()
    os.chdir(path)

    try: yield
    finally: os.chdir(path_cwd)

def download_and_unzip():
    if (DIR_DATA / 'train').exists(): return

    with workdir(DIR_DATA):
        shrun(f'kaggle competitions download -c {COMPETITION}')
        for filename in DIR_DATA.glob('*.zip'):
            shrun(f'unzip {filename} && rm {filename}')

def copyfiles(path_from, path_to, frac):
    filenames = list(path_from.glob('*'))
    num_files = int(len(filenames) * frac)

    filenames = np.random.choice(filenames, size=num_files, replace=False)

    for filename in tqdm(filenames): shutil.copy(filename, path_to / filename.name)

def create_sample(frac, train=True):
    mode = 'train' if train else 'test'

    DIR_ = DIR_DATA / mode
    DIR_SAMPLE_ = (DIR_DATA_SAMPLE / mode).make()
    if is_empty(DIR_SAMPLE_.glob('*.jpg')): copyfiles(DIR_, DIR_SAMPLE_, frac)

def make_symlinks():
    for file in DIR_DATA.glob('*.*'):
        if file.stem == 'labels' or (DIR_DATA_SAMPLE / file.name).exists(): continue

        (DIR_DATA_SAMPLE / file.name).symlink_to(file)

def subsample_labels():
    if (DIR_DATA_SAMPLE / 'labels.csv').exists(): return

    labels = pd.read_csv(DIR_DATA / 'labels.csv')

    sample_filenames = (DIR_DATA_SAMPLE / 'train').glob('*.jpg')
    sample_labels = labels.loc[labels['id'].isin((filename.stem
                                                  for filename in sample_filenames))]

    sample_labels.to_csv(DIR_DATA_SAMPLE / 'labels.csv', index=False)