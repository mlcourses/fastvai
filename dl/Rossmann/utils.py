import subprocess, feather

from fastai.imports import *
from fastai.structured import *
from fastai.dataset import *
from fastai.column_data import *

from contextlib import contextmanager
from pathlib import Path

shrun = lambda cmd: print(subprocess.getoutput(cmd))

def _make(self):
    self.mkdir(parents=True, exist_ok=True)
    return self
Path.make = _make

COMPETITION = 'rossmann-store-sales'
DIR_DATA = (Path('~/.kaggle/Competitions').expanduser() / COMPETITION).make()
DIR_CHECKPOINTS = (Path.cwd() / 'Checkpoints').make()

def display_all(*items):
    for i in items: display(i)

@contextmanager
def workdir(path):
    path_cwd = os.getcwd()
    os.chdir(path)

    try: yield
    finally: os.chdir(path_cwd)

@workdir(DIR_DATA)
def download_and_unzip():
    if Path('train.csv').exists(): return

    shrun(f'kaggle competitions download -c {COMPETITION}')
    for filename in Path('').glob('*.zip'):
        shrun(f'unzip {filename} && rm {filename}')

    shrun('wget http://files.fast.ai/part2/lesson14/rossmann.tgz')
    shrun('tar -xzf rossmann.tgz && rm rossmann.tgz')

def join_df(left, right, left_on, right_on=None, suffix='_y', null_test=None):
    if right_on is None: right_on = left_on

    joined = left.merge(right, how='left', left_on=left_on, right_on=right_on,
                      suffixes=("", suffix))

    try:
        if null_test is not None:
            len_null = len(joined[joined[null_test].isnull()])

            if len_null == 0: print('Joined sucessfully')
            else: print(f'Joined. However, {len_null} out of {len(joined)} rows are null!')

        else: print('Joined sucessfully')

    finally: return joined

def inv_y(a): return np.exp(a)

def exp_rmspe(y_pred, targ):
    targ = inv_y(targ)
    pct_var = (targ - inv_y(y_pred))/targ
    return math.sqrt((pct_var**2).mean())

cat_vars = ['Store', 'DayOfWeek', 'Year', 'Month', 'Day', 'StateHoliday', 'CompetitionMonthsOpen',
    'Promo2Weeks', 'StoreType', 'Assortment', 'PromoInterval', 'CompetitionOpenSinceYear', 'Promo2SinceYear',
    'State', 'Week', 'Events', 'Promo_fw', 'Promo_bw', 'StateHoliday_fw', 'StateHoliday_bw',
    'SchoolHoliday_fw', 'SchoolHoliday_bw']

contin_vars = ['CompetitionDistance', 'Max_TemperatureC', 'Mean_TemperatureC', 'Min_TemperatureC',
   'Max_Humidity', 'Mean_Humidity', 'Min_Humidity', 'Max_Wind_SpeedKm_h',
   'Mean_Wind_SpeedKm_h', 'CloudCover', 'trend', 'trend_DE',
   'AfterStateHoliday', 'BeforeStateHoliday', 'Promo', 'SchoolHoliday']
